"""{{ cookiecutter.description }}"""
VERSION = ({{ cookiecutter.version.split('.')[0] }}, {{ cookiecutter.version.split('.')[1] }}, {{ cookiecutter.version.split('.')[2] }})

__title__ = '{{ cookiecutter.repo_name }}'
__description__ = '{{ cookiecutter.description }}'
__url__ = 'http://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}'
__download_url__ = 'https://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}/repository/archive.tar.gz?ref=master'
__version__ = '.'.join(map(str, VERSION))
__author__ = '{{ cookiecutter.author }}'
__author_email__ = '{{ cookiecutter.email }}'
__license__ = '{{ cookiecutter.license }}'
__copyright__ = "Copyright {% now 'local', '%Y' %} {{ cookiecutter.author }}"
