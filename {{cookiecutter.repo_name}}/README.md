# {{ cookiecutter.project_name }} [![PyPI version](https://badge.fury.io/py/{{ cookiecutter.repo_name }}.svg)](https://badge.fury.io/py/{{ cookiecutter.repo_name }}) {% if cookiecutter.use_pytest == 'y' -%} [![Pipeline status](https://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}/badges/master/pipeline.svg)](https://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}/commits/master) [![Coverage report](https://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}/badges/master/coverage.svg)](https://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}/commits/master) {% endif %}

{{ cookiecutter.description }}

## Features
- TODO

## Installation
{{ cookiecutter.project_name }} requires Python 3.5 or newer to run.

**Python package**

You can easily install {{ cookiecutter.project_name }} using pip:

`pip3 install {{ cookiecutter.repo_name }}`

**Manual**

Alternatively, to get the latest development version, you can clone this repository and then manually install it:

```
git clone git@gitlab.com:{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}.git
cd {{ cookiecutter.repo_name }}
python3 setup.py install
```

## Usage
- TODO

## TODO
- TODO

## Contributing
For information on how to contribute to the project, please check the [Contributor's Guide][contributing].

## Contact
[{{ cookiecutter.email }}](mailto:{{ cookiecutter.email }})

[incoming+{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}@gitlab.com](incoming+{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}@gitlab.com)

## License
{{ cookiecutter.license }}

## Credits
This package was created with [Cookiecutter][cookiecutter] and the [python-cookiecutter][python-cookiecutter] project template.

[contributing]: https://gitlab.com/{{ cookiecutter.gitlab_name }}/{{ cookiecutter.repo_name }}/blob/master/CONTRIBUTING.md
[cookiecutter]: https://github.com/audreyr/cookiecutter
[documentation]: https://{{ cookiecutter.gitlab_name }}.gitlab.io/{{ cookiecutter.repo_name }}
[python-cookiecutter]: https://gitlab.com/radek-sprta/python-cookiecutter
