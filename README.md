# Python Cookiecutter
Cookiecutter template for Python projects. Saves time by bootstrapping your repository.

## Features
- Choose license
- Create setup.py and source path
- Setup tox environment and Gitlab CI
- Prepare documentation

## Usage
To use the template, you have to install Cookiecutter. Simply use pip for the task:

`pip install cookiecutter`

And then run it to bootstrap your project:

`cookiecutter gl:radek-sprta/python-cookiecutter`

For more info, refer to the [documentation][documentation].

## Contributing
For information on how to contribute to the project, please check the [Contributor's Guide][contributing].

## Contact
[mail@radeksprta.eu](mailto:mail@radeksprta.eu)

[incoming+radek-sprta/python-cookiecutter@gitlab.com](incoming+radek-sprta/python-cookiecutter@gitlab.com)

## License
MIT License

## Credits

This package was created with [Cookiecutter][cookiecutter] and the [python-cookiecutter][python-cookiecutter] project template.

[contributing]: https://gitlab.com/radek-sprta/python-cookiecutter/blob/master/CONTRIBUTING.md
[cookiecutter]: https://github.com/audreyr/cookiecutter
[documentation]: https://radek-sprta.gitlab.io/python-cookiecutter
[python-cookiecutter]: https://gitlab.com/radek-sprta/python-cookiecutter
